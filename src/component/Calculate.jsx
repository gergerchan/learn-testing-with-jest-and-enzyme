import { useState } from "react"
const Calculate = () => {
    const [inputX, setInputX] = useState(0)
    const [inputY, setInputY] = useState(0)
    const [operator, setOperator] = useState("+")
    const [hasil, setHasil] = useState(0)

    const hitungHasil = () => {
        if (operator === "+") {
            setHasil(parseInt(inputX)+parseInt(inputY))
        } else if (operator === "-") {
            setHasil(parseInt(inputX)-parseInt(inputY))
        } else if (operator === "*") {
            setHasil(parseInt(inputX)*parseInt(inputY))
        } else if (operator === "/") {
            setHasil(parseInt(inputX)/parseInt(inputY))
        }
    }

    return (
        <div>
            <h1 className="title">Aplikasi Calculator Gerry</h1>
            <h3 id="hasil">{hasil}</h3>
            <input type="text" id="inputX" placeholder="0" value={inputX} onChange={(e)=> setInputX(e.target.value)}/>
            <select id="operator" value={operator} onChange={(e)=> setOperator(e.target.value)}>
                <option value="+" selected>Tambah (+)</option>
                <option value="-">Kurang (-)</option>
                <option value="*">Kali (*)</option>
                <option value="/">Bagi (/)</option>
            </select>
            <input type="text" id="inputY" placeholder="0" value={inputY} onChange={(e)=> setInputY(e.target.value)} />
            <button onClick={hitungHasil}>Hitung</button>

        </div>
    )
}

export default Calculate

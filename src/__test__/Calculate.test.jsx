import Calculate from "../component/Calculate"

import { configure, shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

configure({ adapter: new Adapter() });

let wrapper
beforeEach(() => {
    wrapper = shallow(<Calculate />)
})

test("Title pada Calculate", () => {
    expect(wrapper.find(".title").text()).toContain("Aplikasi Calculator")
}) 

test("Hasil Calculate",() => {
    expect(wrapper.find("#hasil").text()).toBe("0")
})

test("Button Hitung", () => {
    expect(wrapper.find("button").text()).toBe("Hitung")
})

test("Input X", () => {
    expect(wrapper.find("#inputX").props().value).toBe(0)
})

test("Input Y", () => {
    expect(wrapper.find("#inputY").props().value).toBe(0)
})

test("Operator", () => {
    expect(wrapper.find("#operator").props().value).toBe("+")
} )

test("Merubah nilai x", () => {
    wrapper.find("#inputX").simulate("change", {
        target:{value:"5"}
    })
    expect(wrapper.find("#inputX").props().value).toBe("5")

    wrapper.find("#inputX").simulate("change", {
        target:{value:"50"}
    })
    expect(wrapper.find("#inputX").props().value).toBe("50")
})

test("Merubah nilai y", () => {
    wrapper.find("#inputY").simulate("change", {
        target:{value:"32"}
    })
    expect(wrapper.find("#inputY").props().value).toBe("32")
    wrapper.find("#inputY").simulate("change", {
        target:{value:"23"}
    })
    expect(wrapper.find("#inputY").props().value).toBe("23")
})

test("Merubah nilai Operator", () => {
    wrapper.find("#operator").simulate("change", {
        target:{value:"-"}
    })
    expect(wrapper.find("#operator").props().value).toBe("-")
    wrapper.find("#operator").simulate("change", {
        target:{value:"/"}
    })
    expect(wrapper.find("#operator").props().value).toBe("/")
})

test("Menghitung hasil pertambahan", () => {
    // simulasi 5+32=37
    wrapper.find("#inputX").simulate("change", {
        target:{value:"5"}
    })
    wrapper.find("#operator").simulate("change", {
        target:{value:"+"}
    })
    wrapper.find("#inputY").simulate("change", {
        target:{value:"32"}
    })
    wrapper.find("button").simulate("click")
    expect(wrapper.find("#hasil").text()).toBe("37")

    // simulasi 124+63=187
    wrapper.find("#inputX").simulate("change", {
        target:{value:"124"}
    })
    wrapper.find("#operator").simulate("change", {
        target:{value:"+"}
    })
    wrapper.find("#inputY").simulate("change", {
        target:{value:"63"}
    })
    wrapper.find("button").simulate("click")
    expect(wrapper.find("#hasil").text()).toBe("187")
})

test("Menghitung hasil pengurangan", () => {
    // simulasi 5-32=-27
    wrapper.find("#inputX").simulate("change", {
        target:{value:"5"}
    })
    wrapper.find("#operator").simulate("change", {
        target:{value:"-"}
    })
    wrapper.find("#inputY").simulate("change", {
        target:{value:"32"}
    })
    wrapper.find("button").simulate("click")
    expect(wrapper.find("#hasil").text()).toBe("-27")

    // simulasi 124-63=61
    wrapper.find("#inputX").simulate("change", {
        target:{value:"124"}
    })
    wrapper.find("#operator").simulate("change", {
        target:{value:"-"}
    })
    wrapper.find("#inputY").simulate("change", {
        target:{value:"63"}
    })
    wrapper.find("button").simulate("click")
    expect(wrapper.find("#hasil").text()).toBe("61")
})

test("Menghitung hasil perkalian", () => {
    // simulasi 5*32=-160
    wrapper.find("#inputX").simulate("change", {
        target:{value:"5"}
    })
    wrapper.find("#operator").simulate("change", {
        target:{value:"*"}
    })
    wrapper.find("#inputY").simulate("change", {
        target:{value:"32"}
    })
    wrapper.find("button").simulate("click")
    expect(wrapper.find("#hasil").text()).toBe("160")

    // simulasi 124*63=7812
    wrapper.find("#inputX").simulate("change", {
        target:{value:"124"}
    })
    wrapper.find("#operator").simulate("change", {
        target:{value:"*"}
    })
    wrapper.find("#inputY").simulate("change", {
        target:{value:"63"}
    })
    wrapper.find("button").simulate("click")
    expect(wrapper.find("#hasil").text()).toBe("7812")
})

test("Menghitung hasil pembagian", () => {
    // simulasi 5/32=0.15625
    wrapper.find("#inputX").simulate("change", {
        target:{value:"5"}
    })
    wrapper.find("#operator").simulate("change", {
        target:{value:"/"}
    })
    wrapper.find("#inputY").simulate("change", {
        target:{value:"32"}
    })
    wrapper.find("button").simulate("click")
    expect(wrapper.find("#hasil").text()).toBe("0.15625")

    // simulasi 124/63=1,96825397
    wrapper.find("#inputX").simulate("change", {
        target:{value:"124"}
    })
    wrapper.find("#operator").simulate("change", {
        target:{value:"/"}
    })
    wrapper.find("#inputY").simulate("change", {
        target:{value:"63"}
    })
    wrapper.find("button").simulate("click")
    expect(wrapper.find("#hasil").text()).toBe("1.9682539682539681")
})